export const backToTop = () => {
  $(window).scrollTop() > $('.siteHeader').outerHeight() ? $('.siteFooter__toTop').fadeIn('fast'): $('.siteFooter__toTop').fadeOut('fast');
  $(window).scrollTop() > ($('.topPage').height() - $(window).height() - 100) ? $('.siteFooter__toTop').addClass('u-bottom'): $('.siteFooter__toTop').removeClass('u-bottom');
};
